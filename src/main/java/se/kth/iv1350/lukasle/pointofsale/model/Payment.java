package se.kth.iv1350.lukasle.pointofsale.model;

import java.math.BigDecimal;

/**
 * Represents a transation request from a card terminal
 */
public class Payment {

    public enum PaymentType{
        CREDITCARD, DEBITCARD
    }

    private BigDecimal amount;
    private PaymentType paymentType;

    /**
     *
     * @param amount to be payed
     * @param paymentType of transaction
     */
    public Payment(BigDecimal amount, PaymentType paymentType){
        this.amount = amount;
        this.paymentType = paymentType;
    }

    /**
     * @return amount of this payment
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @return type of this payment
     */
    public PaymentType getPaymentType() {
        return paymentType;
    }
}
