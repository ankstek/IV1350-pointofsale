package se.kth.iv1350.lukasle.pointofsale.integration;

import se.kth.iv1350.lukasle.pointofsale.model.Payment;

/**
 * Handles the the accounting systems and interacts with them. The systems are mocked.
 * This is a singleton class, use getInstance()
 */

public class AccountingHandler {

    private static AccountingHandler ah = new AccountingHandler();

    private AccountingHandler(){}

    /**
     *
     * @return returns the singleton instance of AccountingHandler
     */
    public static AccountingHandler getInstance() {
        return ah;
    }

    /**
     * Handles credit card payments
     * @param payment A credit card payment with transaction data
     * @return whether the transaction succeeded or not
     */
    public boolean processPayment(Payment payment){
        System.out.println("Card terminal: Processing transaction with " + payment.getPaymentType() + "...");
        System.out.println("Card terminal: Transaction of " + payment.getAmount() + " completed successfully!");
        return true;
    }
}
