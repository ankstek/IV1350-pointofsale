package se.kth.iv1350.lukasle.pointofsale.integration;

/**
 * This exception is thrown by the database handler when an Item with an identifier is not available in the item database.
 */
public class ItemNotFoundException extends Exception  {

    public ItemNotFoundException(String message){
        super(message);
    }

    public ItemNotFoundException(){
        super();
    }


}
