package se.kth.iv1350.lukasle.pointofsale.view;

import se.kth.iv1350.lukasle.pointofsale.model.Sale;

import java.math.BigDecimal;
import java.util.Observable;
import java.util.Observer;

/**
 * Observer class that observes a sale and is notified when the sale is complete.
 */
public class TotalRevenueView implements Observer {

    private BigDecimal revenueTotal = new BigDecimal(0);

    public TotalRevenueView(){

    }

    /**
     * Updates the total revenue when it is notified.
     * @param o the observed object
     * @param arg unused, part of the Observer interface
     */
    @Override
    public void update(Observable o, Object arg) {
        if (o.getClass() == Sale.class){
            Sale sale = (Sale) o;
            revenueTotal = revenueTotal.add(sale.getTotal());
            System.out.println("Total revenue: " + revenueTotal.toPlainString());
        } else {
            System.out.println("Updated observer but not Sale class");
        }
    }
}
