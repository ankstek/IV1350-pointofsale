package se.kth.iv1350.lukasle.pointofsale.model;

import se.kth.iv1350.lukasle.pointofsale.integration.Item;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Represents a receipt of a sale.
 *
 * @see Sale
 */
public class Receipt {

    private ArrayList<String> itemLines = new ArrayList<>();
    private static String newline = System.getProperty("line.separator");
    private String timeStamp;
    private BigDecimal total;
    private BigDecimal tax;
    private String HEADER = "Receipt for purchase at PoS POS" + newline + "Items bought:";
    private String FOOTER = "Thank you for shopping at PoS POS!";
    private String paymentDescription = "";

    public Receipt() {
    }

    /**
     * Formats a row on the receipt based on the item, item quantity and price per item.
     * This implementation is using 48 character per row.
     *
     * 2 characters for quantity
     * 2 characters for '*' and a space
     * 29 characters for description of item
     * 1 character for a space
     * 10 characters for a price per unit of item (1234567.90)
     * 3 characters for a space and 'kr'
     *
     * @param item the item which data should be formatted from
     */
    public void addItemRow(Item item) {

        StringBuilder sb = new StringBuilder();

        if (item.getQuantity() > 9) {
            sb.append(item.getQuantity() + "* ");
        } else {
            sb.append(item.getQuantity() + "*  ");
        }

        String desc = item.getDescription();
        if (item.getDescription().length() < 29) {
            while (desc.length() < 29) {
                desc += " ";
            }

        } else if (item.getDescription().length() > 29) {
            desc = desc.substring(0, 29);
        }
        desc += " ";

        sb.append(desc);

        String price = item.getPricePerUnit().toPlainString();
        int offset = 10 - price.length();

        for (int i = 0; i < offset; i++) {
            sb.append(" ");
        }
        sb.append(price + " kr");

        itemLines.add(sb.toString());
    }

    /**
     * Set the timestamp for when the purchase was made. Accuracy of time stamp is store specific.
     * @param timeStamp string representation of a timestamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Set the total of the sale
     * @param total
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * Set the total tax of the sale
     * @param tax
     */
    public void setTotalTax(BigDecimal tax) {
        this.tax = tax;
    }

    /**
     * Set how the sale was payed for.
     * @param paymentDescription
     */
    public void setPayment(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    /**
     * Returns a pre-formatted string that can be printed as it is without any changes made to it.
     * @return formatted string
     */
    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(HEADER + newline);

        for (String str : itemLines) {
            stringBuilder.append(str);
            stringBuilder.append(newline);
        }

        stringBuilder.append("Total: " + total + newline);
        stringBuilder.append("Total tax: " + tax + newline);
        stringBuilder.append("Payed with " + paymentDescription);
        stringBuilder.append(newline);
        stringBuilder.append("Purchase occured on" + newline);
        stringBuilder.append(timeStamp);
        stringBuilder.append(FOOTER);

        return stringBuilder.toString();
    }
}
