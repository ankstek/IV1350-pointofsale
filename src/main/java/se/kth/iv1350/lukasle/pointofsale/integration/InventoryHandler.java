package se.kth.iv1350.lukasle.pointofsale.integration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * This class handles a inventory data base. The data base is mocked. This is a singleton class, use getInstance().
 */
public class InventoryHandler {

    private static InventoryHandler ih = new InventoryHandler();

    private InventoryHandler(){}

    /**
     * @return the singleton instance of InventoryHandler class
     */
    public static InventoryHandler getInstance() {
        return ih;
    }

    /**
     * Checks the database for the specified item.
     * @param identifier for the item
     * @return an Item object containing data for the requested item
     * @throws ItemNotFoundException if there is not item corresponding to the identifier
     * @throws DatabaseUnavailableException if there was an error where the database did not respond correctly
     */
    public Item lookUpItem(String identifier) throws ItemNotFoundException, DatabaseUnavailableException {

        String description;
        BigDecimal pricePerUnit;
        if (identifier.length() == 0){
            identifier = " ";
        }
        switch (identifier.charAt(0)) {
            case 'a':
                description = "apelsin";
                pricePerUnit = new BigDecimal(13.37).setScale(2, RoundingMode.CEILING);
                break;
            case 'd':
                description = "daim - smak av demon";
                pricePerUnit = new BigDecimal(6.66666666666).setScale(2, RoundingMode.CEILING);
                break;
            case 'b':
                description = "banan";
                pricePerUnit = new BigDecimal(4.200000).setScale(2, RoundingMode.CEILING);
                break;
            case 'x':
                throw new DatabaseUnavailableException("Error: Internal data base error");
            default:
                throw new ItemNotFoundException("Error: Invalid item identifier");
        }

        return new Item(identifier, 1, description, pricePerUnit, 12);
    }

    /**
     * Updates the inventory system so that the store keeps an accurate inventory
     * @param itemsBought - list of items that was bought
     */
    public void updateInventory(List<Item> itemsBought) {}
}
