package se.kth.iv1350.lukasle.pointofsale.controller;

import se.kth.iv1350.lukasle.pointofsale.model.Payment;

import java.math.BigDecimal;

/**
 * Interface of Controller implementations for POS.
 */

public interface ControllerInterface {

    /**
     * Starts a new sale in the model
     */
    void startNewSale();

    /**
     * modfies amount of a certain item
     * @param id item id to be modified
     * @param amount the amount to set the item to
     */
    String modifyAmount(String id, int amount);

    /**
     * end the sale and present customer with different options for payment
     */
    String finishSale();

    /**
     * customer chose to pay with cash
     * @param amount amount of cash the customer presented the cashier with
     * @return the change after the total amount have been subtracted
     */
    BigDecimal payCash(BigDecimal amount);

    /**
     * The customer chose to pay with card, send it to the accounting system to get it processed
     * @param pt the transaction info from the card terminal
     */
    String payCard(Payment.PaymentType pt);

    /**
     * When a new item has been added to a sale this method is called and provided with
     * the amount of the item and the id of the item.
     * @param identifier id of the item
     * @param amount amount of the item
     */
    String enterItemManually(String identifier, int amount);
}
