package se.kth.iv1350.lukasle.pointofsale.model;

import java.math.BigDecimal;

/**
 * The cash register system, maintains a balance of cash and updates it when payments with cash is made
 */
public class CashRegister {

    private BigDecimal balance;

    /**
     * @param startingCash how much the register starts with when store opens
     */
    public CashRegister(BigDecimal startingCash) {
        this.balance = startingCash;
    }

    /**
     * Calculates change for cash payments
     *
     * @param total what the total is for the current sale
     * @param cash  how much the customer payed
     * @return the change (cash - total)
     */
    public BigDecimal calculateChange(BigDecimal total, BigDecimal cash) {
        return cash.subtract(total);
    }

    /**
     * Put the amount of cash in the register
     *
     * @param amount the amount of cash deposited in the register
     */
    public void depositCash(BigDecimal amount) {
        balance = balance.add(amount);
    }

    /**
     * @return the current balance of the register
     */
    public BigDecimal getBalance() {
        return balance;
    }
}
