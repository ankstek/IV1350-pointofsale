package se.kth.iv1350.lukasle.pointofsale.model;

import se.kth.iv1350.lukasle.pointofsale.integration.*;
import se.kth.iv1350.lukasle.pointofsale.view.TotalRevenueView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This represents an ongoing sale. Items are stored here and the class provides methods
 * for calculating tax, total price, generating receipt information and notifying any observers attached to it.
 */
public class Sale extends Observable {

    private HashMap<String, Item> goods = new HashMap<>();
    private Logger log = Logger.getLogger("main");

    /**
     * Constructor of sale, should be started by the controller.
     *
     * @param trv TotalRevenueView observer
     */
    public Sale(TotalRevenueView trv) {
        this.addObserver(trv);
    }

    /**
     * Adds an item to the hash map of current items of a sale. The item is looked up in the InventoryHandler.
     * On a successful lookup a message of the amount of item and item description and price per unit is returned,
     * otherwise an error is returned.
     *
     * @param identifier id of the item
     * @param amount     amount of the specific item
     * @return string of item description, amount and price per unir OR an error message with prefix 'Error:'
     */
    public String addItem(String identifier, int amount) {
        if (goods.containsKey(identifier)) {
            goods.get(identifier).setQuantity(goods.get(identifier).getQuantity() + amount);
        } else {
            try {
                Item newItem = InventoryHandler.getInstance().lookUpItem(identifier);
                newItem.setQuantity(amount);
                goods.put(identifier, newItem);
            } catch (ItemNotFoundException i) {
                log.log(Level.WARNING, i.getMessage());
                return i.getMessage();
            } catch (DatabaseUnavailableException d) {
                log.log(Level.SEVERE, d.getMessage());
                synchronized (this) {
                    for (StackTraceElement traceElement : d.getStackTrace())
                        log.log(Level.SEVERE, "\tat " + traceElement);
                }
                return d.getMessage();
            }
        }
        return amount + "* " + goods.get(identifier).getDescription() + " á " + goods.get(identifier).getPricePerUnit() + " kr";
    }

    /**
     * Edit the amount of an item in a sale. If set to 0, remove the item from the sale.
     *
     * @param identifier id of the item
     * @param amount     of the item
     */
    public String editItem(String identifier, int amount) {
        String itemInfo = "Error: not such item in current sale!";
        if (goods.containsKey(identifier)) {
            if (amount <= 0) {
                 itemInfo = 0 + "* " + goods.get(identifier).getDescription() + " á " + goods.get(identifier).getPricePerUnit() + " kr";
                goods.remove(identifier);
            } else {
                itemInfo = amount + "* " + goods.get(identifier).getDescription() + " á " + goods.get(identifier).getPricePerUnit() + " kr";
                goods.get(identifier).setQuantity(amount);
            }
        }

        return itemInfo;
    }

    /**
     * Prepares a Receipt file to be sent to the printer and then sends it.
     *
     * @param printer            the printer that should print the receipt
     * @param paymentDescription "CASH" or a Payment.PaymentType enum
     * @see Printer
     */
    public void printReceipt(Printer printer, String paymentDescription) {
        Set<Map.Entry<String, Item>> setOfItems = goods.entrySet();
        Receipt receipt = new Receipt();

        BigDecimal total = getTotal().setScale(2, RoundingMode.CEILING);
        BigDecimal totalTax = new BigDecimal(0);

        for (Map.Entry<String, Item> setEntry : setOfItems) {
            receipt.addItemRow(setEntry.getValue());
            totalTax = totalTax.add(calculateTax(setEntry.getValue()));
        }

        receipt.setTimeStamp(new Timestamp(System.currentTimeMillis()).toLocalDateTime().toString());
        receipt.setPayment(paymentDescription);
        receipt.setTotal(total);
        receipt.setTotalTax(totalTax);

        printer.printReceipt(receipt);
    }

    /**
     * Calculates the total tax of a specific item based on its price per unit and tax percentage.
     *
     * @param item to calculate tax for
     * @return the total tax
     */
    private BigDecimal calculateTax(Item item) {
        BigDecimal itemTax = item.getPricePerUnit().multiply(new BigDecimal(item.getQuantity()));
        return itemTax = itemTax.divide(new BigDecimal(100)).multiply(new BigDecimal(item.getTaxPercentage())).setScale(2, RoundingMode.CEILING);
    }

    public ArrayList<Item> getItemList() {
        Set<Map.Entry<String, Item>> setOfItems = goods.entrySet();
        ArrayList<Item> itemList = new ArrayList<>();

        for (Map.Entry<String, Item> setEntry : setOfItems) {
            itemList.add(setEntry.getValue());
        }
        return itemList;
    }

    /**
     * Notify the observers for this class that its status has been changed.
     */
    public void notifyView() {
        this.setChanged();
    }

    /**
     * Get the total cost of all items currently added to the sale.
     *
     * @return the total cost
     */
    public BigDecimal getTotal() {
        Set<Map.Entry<String, Item>> setOfItems = goods.entrySet();
        BigDecimal total = new BigDecimal(0);

        for (Map.Entry<String, Item> setEntry : setOfItems) {
            BigDecimal bd = setEntry.getValue().getPricePerUnit().multiply(new BigDecimal(setEntry.getValue().getQuantity()));
            total = total.add(bd);
        }
        return total.setScale(2, RoundingMode.CEILING);
    }
}
