package se.kth.iv1350.lukasle.pointofsale.view;

import se.kth.iv1350.lukasle.pointofsale.controller.Controller;
import se.kth.iv1350.lukasle.pointofsale.model.Payment;

import java.math.BigDecimal;

/**
 * Mock class for view, represents a user interface.
 */
public class View {

    private Controller controller;

    public View(Controller controller){
        this.controller = controller;
    }

    private void printLine(String message) {
        System.out.println(message);
    }

    public void startNewSale(){
        controller.startNewSale();
    }

    public void finishSale(){
        printLine(controller.finishSale());
    }

    public void scanItem(String id, int amount){
        printLine(controller.enterItemManually(id, amount));
    }

    public void editItem(String id, int amount){
        printLine(controller.modifyAmount(id, amount));
    }

    public void payCash(BigDecimal amount){
        printLine("Change: " + controller.payCash(amount));
    }

    public void payCard(Payment.PaymentType pt){
        printLine(controller.payCard(pt));
    }
}