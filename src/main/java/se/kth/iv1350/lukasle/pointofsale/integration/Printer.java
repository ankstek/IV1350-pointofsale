package se.kth.iv1350.lukasle.pointofsale.integration;

import se.kth.iv1350.lukasle.pointofsale.model.Receipt;

/**
 * Prints receipts. Does not do any formatting, this must be done by the receipt. This is so that the printer can be a
 * receipt printer or a regular printer that prints receipts on an A4 for example.
 *
 * @see Receipt
 */
public class Printer {

    public Printer() {
    }

    /**
     * Print the receipt in the printer
     * @param receipt to be printed
     */
    public void printReceipt(Receipt receipt) {
        System.out.println("RECEIPT FROM PRINTER:");
        System.out.println(receipt.toString());
    }
}
