package se.kth.iv1350.lukasle.pointofsale.integration;

import java.math.BigDecimal;

/**
 * An object representing an item of a Sale.
 *
 * @see se.kth.iv1350.lukasle.pointofsale.model.Sale
 */
public class Item {

    private String identifier = "UNKNOWN";
    private int quantity = -1;
    private String description = "NOT_AVAILABLE";
    private BigDecimal pricePerUnit = null;
    private double taxPercentage = 12;

    /**
     *
     * @param identifier the id of the item, used for database lookup
     * @param quantity the quantity of the item in a sale
     * @param description of the item
     * @param pricePerUnit of the item in <insert currency here>
     * @param taxPercentage of the current item (moms)
     */
    public Item(String identifier, int quantity, String description, BigDecimal pricePerUnit, double taxPercentage) {
        this.description = description;
        this.identifier = identifier;
        this.pricePerUnit = pricePerUnit;
        this.quantity = quantity;
        this.taxPercentage = taxPercentage;
    }

    public String getIdentifier() {
        return identifier;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTaxPercentage() {
        return taxPercentage;
    }

}
