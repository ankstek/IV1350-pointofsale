package se.kth.iv1350.lukasle.pointofsale.integration;

/**
 * Exception thrown when the data base could not be reached or didn't provide an item.
 */
public class DatabaseUnavailableException extends Exception {

    /**
     * @param message informative message of why this exception was thrown.
     */
    public DatabaseUnavailableException(String message){
        super(message);
    }

    public DatabaseUnavailableException(){
        super();
    }
}
