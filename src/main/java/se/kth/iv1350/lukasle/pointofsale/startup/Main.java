package se.kth.iv1350.lukasle.pointofsale.startup;

import se.kth.iv1350.lukasle.pointofsale.controller.Controller;
import se.kth.iv1350.lukasle.pointofsale.integration.Printer;
import se.kth.iv1350.lukasle.pointofsale.model.CashRegister;
import se.kth.iv1350.lukasle.pointofsale.model.Payment;
import se.kth.iv1350.lukasle.pointofsale.view.TotalRevenueView;
import se.kth.iv1350.lukasle.pointofsale.view.View;

import java.math.BigDecimal;

public class Main {

    /**
     * System demo of PoS POS, shows all functionality of the PoS POS system.
     *
     * BASIC FLOW, TWO SALES:
     * - System setup
     *
     * - start sale 1
     * - add 2 oranges
     * - add 4 bananas, 4 times
     * - set the amount of oranges to 1
     * - add and remove a daim
     * - enter an invalid item ID
     * - enter an item that will trigger a database "crash"
     * - finish sale
     * - customer pays by card
     *
     * - start sale 2
     * - add 5 oranges
     * - add 4 bananas
     * - add 1 daim
     * - finish sale
     * - customer pays by cash
     *
     * @param args unused
     */
    public static void main(String[] args){

        TotalRevenueView trv = new TotalRevenueView();
        CashRegister cr = new CashRegister(new BigDecimal(1000));
        Printer printer = new Printer();
        Controller controller = new Controller(printer, trv, cr);
        View view = new View(controller);

        view.startNewSale();
        view.scanItem("apelsin", 2);
        view.scanItem("banan", 4);
        view.scanItem("banan", 4);
        view.scanItem("banan", 4);
        view.scanItem("banan", 4);
        view.editItem("apelsin", 1);
        view.scanItem("daim", 1);
        view.editItem("daim", 0);
        view.scanItem("", 1);
        view.scanItem("x", 1);
        view.finishSale();
        view.payCard(Payment.PaymentType.CREDITCARD);

        view.startNewSale();
        view.scanItem("apelsin", 5);
        view.scanItem("banan", 4);
        view.scanItem("daim", 1);
        view.finishSale();
        view.payCash(new BigDecimal(300));
    }
}
