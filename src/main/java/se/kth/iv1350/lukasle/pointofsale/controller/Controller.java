package se.kth.iv1350.lukasle.pointofsale.controller;

import se.kth.iv1350.lukasle.pointofsale.integration.AccountingHandler;
import se.kth.iv1350.lukasle.pointofsale.integration.InventoryHandler;
import se.kth.iv1350.lukasle.pointofsale.integration.Printer;
import se.kth.iv1350.lukasle.pointofsale.model.CashRegister;
import se.kth.iv1350.lukasle.pointofsale.model.Payment;
import se.kth.iv1350.lukasle.pointofsale.model.Sale;
import se.kth.iv1350.lukasle.pointofsale.view.TotalRevenueView;

import java.math.BigDecimal;

/**
 * The controller class that sits between model and view.
 *
 * @author lukasle@kth.se
 */

public class Controller implements ControllerInterface {

    private Sale currentSale = null;
    private Printer printer;
    private TotalRevenueView trv;
    private CashRegister cashRegister;
    private static String newline = System.getProperty("line.separator");

    /**
     * Controller needs a Printer to print receipts, a TRV observer for keeping the revenue updated and a cash register
     *
     * @param printer      the printer object
     * @param trv          the observer class for updating the revenue
     * @param cashRegister the cash register
     */
    public Controller(Printer printer, TotalRevenueView trv, CashRegister cashRegister) {
        this.printer = printer;
        this.trv = trv;
        this.cashRegister = cashRegister;
    }

    /**
     * Creates a new Sale instance with a TotalRevenueView observer attached to it.
     *
     * @see TotalRevenueView
     */
    public void startNewSale() {
        currentSale = new Sale(trv);
    }

    /**
     * Set the quantity of an Item in the Item HashMap of Sale to @param amount
     *
     * @param id     id string of the Item
     * @param amount the quantity to set it to
     */
    public String modifyAmount(String id, int amount) {
        return currentSale.editItem(id, amount) + newline + "Total:" + currentSale.getTotal().toPlainString();
    }

    /**
     * Finished the current sale and presents the total. In the view the customer is given the option of paying with cash or by card.
     */
    public String finishSale() {
        return "Total: " + currentSale.getTotal();
    }

    /**
     * The customer has chosen to pay with cash. The controller registers the amount payed in the cash register,
     * calculates change and updates the TotalRevenueView observer and the InventoryHandler.
     *
     * @param amount the cash amount that the customer payed with
     * @return amount of change
     */
    public BigDecimal payCash(BigDecimal amount) {
        BigDecimal change = cashRegister.calculateChange(currentSale.getTotal(), amount);
        cashRegister.depositCash(currentSale.getTotal());

        currentSale.notifyView();
        currentSale.notifyObservers();
        currentSale.printReceipt(printer, "cash");
        InventoryHandler.getInstance().updateInventory(currentSale.getItemList());
        return change;
    }

    /**
     * The customer has chosen to pay with card. The controller sends information about the transaction to the
     * accounting system and updates the TotalRevenueView observer and the InventoryHandler.
     * @param pt the payment type
     */
    public String payCard(Payment.PaymentType pt) {
        Payment payment = new Payment(currentSale.getTotal(), pt);

        AccountingHandler.getInstance().processPayment(payment);

        currentSale.notifyView();
        currentSale.notifyObservers();
        currentSale.printReceipt(printer, pt.toString());
        InventoryHandler.getInstance().updateInventory(currentSale.getItemList());
        return "Charged " + pt.toString() + " with " + currentSale.getTotal().toPlainString() + " SEK.";
    }

    /**
     * The method to add items to an ongoing sale
     * @param identifier the id of the item
     * @param amount the amount of items of the item type
     */
    public String enterItemManually(String identifier, int amount) {
        return currentSale.addItem(identifier, amount) + newline + "Total:" + currentSale.getTotal().toPlainString();
    }
}
