package pointofsale.model;

import org.junit.jupiter.api.Test;
import se.kth.iv1350.lukasle.pointofsale.model.CashRegister;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCashRegister {

    @Test
    public void testCashRegister(){
        assertTrue(new CashRegister(new BigDecimal(10)) instanceof CashRegister);
        assertEquals(new BigDecimal(10), new CashRegister(new BigDecimal(10)).getBalance());
    }

    @Test
    public void testCalculateChange() {
        CashRegister cr = new CashRegister(new BigDecimal(50));
        BigDecimal bd = cr.calculateChange(new BigDecimal(20), new BigDecimal(30));
        assertEquals(new BigDecimal(10), bd);
    }

    @Test
    public void testCalculateNegativeChange() {
        CashRegister cr = new CashRegister(new BigDecimal(50));
        BigDecimal bd = cr.calculateChange(new BigDecimal(40), new BigDecimal(30));
        assertEquals(new BigDecimal(-10), bd);
    }

    @Test
    public void testDepositCash(){
        CashRegister cr = new CashRegister(new BigDecimal(50));
        cr.depositCash(new BigDecimal(10));
        BigDecimal bd = cr.getBalance();
        assertEquals(new BigDecimal(60), bd);
    }

    @Test
    public void testDepositNoCash(){
        CashRegister cr = new CashRegister(new BigDecimal(50));
        cr.depositCash(new BigDecimal(0));
        BigDecimal bd = cr.getBalance();
        assertEquals(new BigDecimal(50), bd);
    }

    @Test
    public void testGetBalance(){
        CashRegister cr = new CashRegister(new BigDecimal(50));
        assertEquals(new BigDecimal(50), cr.getBalance());
    }
}
