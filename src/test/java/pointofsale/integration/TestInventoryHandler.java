package pointofsale.integration;

import org.junit.jupiter.api.Test;
import se.kth.iv1350.lukasle.pointofsale.integration.DatabaseUnavailableException;
import se.kth.iv1350.lukasle.pointofsale.integration.InventoryHandler;
import se.kth.iv1350.lukasle.pointofsale.integration.Item;
import se.kth.iv1350.lukasle.pointofsale.integration.ItemNotFoundException;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

public class TestInventoryHandler {

    @Test
    public void testGetInstance() {
        assertTrue(InventoryHandler.getInstance() instanceof InventoryHandler);
    }

    @Test
    public void testLookUpItemOrange() {
        BigDecimal orangePrice = new BigDecimal(13.37).setScale(2, RoundingMode.CEILING);
        Item orangeItem = new Item("apelsin", 1, "apelsin", orangePrice, 12);

        try {
            Item testItem = InventoryHandler.getInstance().lookUpItem("apelsin");
            assertTrue(testItem instanceof Item);
            assertEquals(testItem.getDescription(), orangeItem.getDescription());
            assertEquals(testItem.getQuantity(), orangeItem.getQuantity());
            assertEquals(testItem.getIdentifier(), orangeItem.getIdentifier());
            assertEquals(testItem.getPricePerUnit().toPlainString(), orangeItem.getPricePerUnit().toPlainString());
        } catch (ItemNotFoundException e) {
            fail("Database failed to find the item");
        } catch (DatabaseUnavailableException e) {
            fail("Identifier started with x");
        }
    }

    @Test
    public void testLookUpItemBanana() {
        BigDecimal bananaPrice = new BigDecimal(4.200000).setScale(2, RoundingMode.CEILING);
        Item bananaItem = new Item("banan", 1, "banan", bananaPrice, 12);

        try {
            Item testItem = InventoryHandler.getInstance().lookUpItem("banan");
            assertTrue(testItem instanceof Item);
            assertEquals(testItem.getDescription(), bananaItem.getDescription());
            assertEquals(testItem.getQuantity(), bananaItem.getQuantity());
            assertEquals(testItem.getIdentifier(), bananaItem.getIdentifier());
            assertEquals(testItem.getPricePerUnit().toPlainString(), bananaItem.getPricePerUnit().toPlainString());
        } catch (ItemNotFoundException e) {
            fail("Database failed to find the item");
        } catch (DatabaseUnavailableException e) {
            fail("Identifier started with x");
        }
    }

    @Test
    public void testItemBananaIsNotAnOrange() {
        BigDecimal orangePrice = new BigDecimal(13.37).setScale(2, RoundingMode.CEILING);
        Item orangeItem = new Item("apelsin", 1, "apelsin", orangePrice, 12);

        try {
            Item testItem = InventoryHandler.getInstance().lookUpItem("banan");
            assertTrue(testItem instanceof Item);
            assertNotEquals(testItem.getDescription(), orangeItem.getDescription());
            assertEquals(testItem.getQuantity(), orangeItem.getQuantity());
            assertNotEquals(testItem.getIdentifier(), orangeItem.getIdentifier());
            assertNotEquals(testItem.getPricePerUnit().toPlainString(), orangeItem.getPricePerUnit().toPlainString());
        } catch (ItemNotFoundException e) {
            fail("Database failed to find the item");
        } catch (DatabaseUnavailableException e) {
            fail("Identifier started with x");
        }
    }

    @Test
    public void throwsExceptionWhenInvalidItemIdIsUsed() {
        try {
            InventoryHandler.getInstance().lookUpItem("");
            fail("Should throw an exception if supplied with an invalid item string");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals("Error: Invalid item identifier", e.getMessage()),
                    () -> assertNull(e.getCause()),
                    () -> assertEquals(ItemNotFoundException.class, e.getClass())
            );
        }
    }

    @Test
    public void throwsExceptionWhenXItemIdIsUsed() {
        try {
            InventoryHandler.getInstance().lookUpItem("x");
            fail("Should throw an exception if supplied with an item string of x");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals("Error: Internal data base error", e.getMessage()),
                    () -> assertNull(e.getCause()),
                    () -> assertEquals(DatabaseUnavailableException.class, e.getClass())
            );
        }
    }
}

